/*
  Clignotement
  Allume la LED 13 (appelée LED_BUILTIN) pendant 1 seconde,
  puis l'éteint pendant 1 seconde.
 
  Cette exemple est dans le domaine public et est disponible dans les exemples de l'IDE Arduino.

*/
 
/*
  La broche 13 a une LED interne connecté sur l'Arduino.
  Il est possible de câbler une LED rouge externe et une resistance de 220 Ohms en série sur une breadboard.
  Broche GND → au - de la LED, le + de la LED → à la résistance → à la broche 13 de l'Arduino.
*/

// déclare une variable nommée led et de type entier, permet de faire référence à la broche 13 dans le programme
int led = 13;

// le setup regroupe les instructions qui seront exécutées au démarrage du programme y compris quand on presse le bouton reset
void setup() {                
  // on initialise la broche 13 en tant que sortie
  pinMode(led, OUTPUT);
}

// contient les instructions que l'on souhaite voir exécutées encore et encore tant que l'Arduino est alimenté
void loop() {
  digitalWrite(led, HIGH);   // allumer la LED (HIGH est le niveau de voltage, état haut)
  delay(1000);               // 1000 millisecondes, attendre une seconde, la valeur peut être changée
  digitalWrite(led, LOW);    // éteindre la LED (LOW est le niveau de voltage, état bas)
  delay(1000);               // attendre une seconde
}
